# Achu Lumas' Personal Page

Visit it at [https://achuluma.gitlab.io](https://achuluma.gitlab.io).

## Contribute

If you encounter any issues or have suggestions for improvements, please, feel
free to [send a Merge Request](https://gitlab.com/ach.lumap/achuluma-gitlab-io/merge_requests)
and/or [open an issue](https://gitlab.com/ach.lumap/achuluma-gitlab-io/issues).

Regarding the formatting of commit messages, this project seeks to follow the
same conventions as git.git, which you can check [here](https://git-scm.com/docs/SubmittingPatches/#describe-changes).
Please, add your Signed-off-by tag to the message body indicating that you agree
with the [Developer Certificate of Origin](https://developercertificate.org/).

## Running

1. [Optional, but recommended] Tell bundle to install dependencies locally: `bundle config --local path "vendor"`
2. Install dependencies: `bundle install`
3. Run Jekyll: `bundle exec jekyll serve`
4. Go to http://localhost:4000

This project uses the [Scribble](https://github.com/muan/scribble) jekyll
template.


