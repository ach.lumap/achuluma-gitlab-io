---
title: "Tags"
layout: default
permalink: /tags/
---

{% capture tags_str %}{{ site.tags | sort_tags }}{% endcapture %}
{% assign tags = tags_str | split:',' %}

<ul class="list pa0">
  {% for tag in tags %}
  <li class="mv2">
    <a class="link blue hover-mid-gray" href="{{ site.url }}/tags/{{ tag }}">{{ tag }}</a>
  </li>
  {% endfor %}
</ul>
