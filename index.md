---
title: About me
---

Intern with Git sponsored by GitLab through the Outreachy program. \\
Passionate about Free/Libre and Open Source Software (FLOSS) and CLI. \\
Currently working on the Outreachy December 2023 cohort project on Git: "Move existing tests to a unit testing framework." \\
**[Resume]({{ site.url }}/resume.pdf)**
{:.mb25}

Some of my interests include FLOSS, CLI, and exploring various aspects of software development.

Here are some of the things I'm currently working on:

- **Outreachy December 2023 Cohort**: I'm interning with Git sponsored by Gitlab through the Outreachy program, focusing on the project to "Move existing tests to a unit testing framework." This involves reworking and transitioning current tests to a more efficient and standardized unit testing framework.
- **Contributions to Open Source**: As an enthusiast of FLOSS, I actively engage in contributing to open-source projects, aiming to improve the usability and functionality of CLI tools.
  
This journey is just getting started, and I'm excited to contribute meaningfully to the open-source community and further explore the world of software development.

## Contact

- Email: ach&#046;<span style="display: none">abcdef</span>.lumap<!-- abcdef -->&#064;gmail&#046;com
- GitHub: [ach.lumap](https://github.com/ach.lumap)
- GitLab: [ach.lumap](https://gitlab.com/ach.lumap)
- LinkedIn: [achu-luma](https://www.linkedin.com/in/achu-luma/)

