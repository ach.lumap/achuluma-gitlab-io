---
layout: posts-by-tag
---

I am participating in [Outreachy December cohort 2023](https://www.outreachy.org/alums/2023-12/#:~:text=skills%3A%20C%2B%2B%2C%20Javascript-,Git,-luma)
contributing to Git. This page will contain the [almost] weekly updates about my
project during the Outreachy period. You may also want to read my
[project proposal](https://lore.kernel.org/git/CAP8UFD0A_vWCZ5cVAZqdTBebdhZNye_FmNNJF+vA7epUx2JWHQ@mail.gmail.com/T/#m62631f8cb734751c8233eb7336668f7f10fb35ae).
