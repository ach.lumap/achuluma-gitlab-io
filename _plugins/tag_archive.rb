# The MIT License (MIT)
#
# Copyright (c) 2008-present Tom Preston-Werner and Jekyll contributors
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Code originally from the Jekyll project, at "docs/_docs/plugins/generators.md",
# commit 9a4b74c44b:
# https://github.com/jekyll/jekyll/blob/9a4b74c44b9679803d907fd97abbd6a84e671c31/docs/_docs/plugins/generators.md
# Modified by Matheus Tavares <matheus.tavb@gmail.com> in Jan 2020.
#

module TagArchive
  class TagPageGenerator < Jekyll::Generator
    safe true

    def generate(site)
      site.tags.each do |tag, posts|
        site.pages << TagPage.new(site, tag, posts)
      end
    end
  end

  class TagPage < Jekyll::Page
    def initialize(site, tag, posts)
      @site = site
      @base = site.source
      @dir  = File.join("tags", tag)
      @name = 'index.html'

      self.process(@name)

      prefaces_dir = File.join(@base, '_tag_pages')
      preface_name = "#{tag}.md"
      if not File.file?(File.join(prefaces_dir, preface_name))
        preface_name = 'default.md'
      end
      self.read_yaml(prefaces_dir, preface_name)

      self.data['title'] = "[#{tag}] posts"
      self.data['posts'] = posts
      self.data['after_title'] = 'link-to-all-posts.html'
    end
  end
end

