
# Checks if og:image properties are valid files and, if so, whether all required
# og:image:* properties are also given.
#
# This file is based on the Stack Overflow answer:
# https://stackoverflow.com/a/43909411/11019779 (under CC BY-SA 4.0) written
# by Christian (https://stackoverflow.com/users/690771/christian)

module Jekyll

  class OGImgPropertiesChecker < Generator
    safe true

    IMG_PROPERTY = 'img'
    REQUIRED = ['img-width', 'img-height', 'img-type']

    def generate(site)
      errors = Array.new
      (site.pages + site.posts).each { |p|
        img_file = p.data[IMG_PROPERTY]
        unless img_file.nil? then
          unless img_file.is_a? String and File.file?(img_file)
            errors << "'#{p.relative_path}': invalid 'img' property: '#{img_file}'"
            next
          end
          REQUIRED.each { |req|
            if p.data[req].nil?
              errors << "'#{p.relative_path}': missing '#{req}'"
            end
          }
        end
      }
      if errors.any?
        msg = "missing img properties:\n"
        errors.each do |error|
          msg += "#{error}\n"
        end
        puts msg
        exit 1
      end
    end

  end
end
