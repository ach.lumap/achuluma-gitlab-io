---
title: "Talks, presentations, etc."
permalink: "/talks/index.html"
---

### MSc in Computer Science

_"Parallelizing Git Checkout: a Case Study of I/O Parallelism on Desktop
Applications"_ <br>
[[Dissertation]]({{ site.url }}{% link assets/msc-dissertation.pdf %}) |
[[Defense Slides]]({{ site.url }}{% link assets/msc-defense-slides.pdf %})

### Undergraduate Capstone Project (TCC)

_"Improving Parallelism in git-grep"_<br>
[[Dissertation]]({{ site.url }}{% link assets/tavares-final-essay.pdf %}) |
[[Poster]]({{ site.url }}{% link assets/tavares-capstone-project-poster.pdf %})

### Others [English]

<ul class="list pa0">

  <li class="mv2">
    <a href="{{ site.url }}{% link slides/git_under_the_hood_en.pdf %}" class="db pv1 link blue hover-mid-gray">
      <time class="fr silver ">Qualcomm techtalk (2022)</time>
	Git "under the hood"
    </a>
  </li>

  <li class="mv2">
    <a href="{{ site.url }}{% link slides/git_101.pdf %}" class="db pv1 link blue hover-mid-gray">
	Git 101 and best commit practices
    </a>
  </li>

  <li class="mv2">
    <a href="{{ site.url }}{% link assets/oop_git_and_kernel.pdf %}" class="db pv1 link blue hover-mid-gray">
      <time class="fr silver ">linuxdev-br (2019)</time>
	Object Oriented Programming in C: A Case Study
    </a>
    <div>
    <a class="arec fr" href="https://www.youtube.com/watch?v=x0ELqk2lCcI">[recording]</a>
    By Matheus and Renato Geh
    </div>
  </li>

  <li class="mv2">
    <a href="{{ site.url }}{% link slides/benefits_of_a_local_community_of_FLOSS_contributors.pdf %}" class="db pv1 link blue hover-mid-gray">
      <time class="fr silver ">debconf (2019)</time>
	Benefits of a Local Community of FLOSS Contributors
    </a>
    <div>
	<a class="arec fr" href="https://debconf19.debconf.org/talks/123-beneficios-de-uma-comunidade-local-de-contribuidores-floss/">[recording (Portuguese)]</a>
	By Marcelo Schmitt, Bruna Bazaluk M V, Anderson Reis Rosa, Matheus Tavares, Wilson Sales, and FLUSP
    </div>
  </li>

  <li class="mv2">
    <a href="{{ site.url }}{% link slides/creating_a_foss_study_group.pdf %}" class="db pv1 link blue hover-mid-gray">
      <time class="fr silver ">linuxdev-br (2019)</time>
	Discussion session: Creating a FOSS Study Group
    </a>
    <div>
	<a class="arec fr" href="https://youtu.be/lGlZmupJDBA">[recording (Portuguese)]</a>
	By André Almeida, Matheus, FLUSP, and LKCamp
    </div>
  </li>

</ul>

### Others [Portuguese]

<ul class="list pa0">

  <li class="mv2">
    <a href="{{ site.url }}{% link slides/git_under_the_hood.pdf %}" class="db pv1 link blue hover-mid-gray">
      <time class="fr silver ">Elo7 techtalk (2022)</time>
	Git "under the hood" (Portuguese)
    </a>
  </li>

  <li class="mv2">
    <a href="{{ site.url }}{% link slides/git_commit_boas_práticas.pdf %}" class="db pv1 link blue hover-mid-gray">
	Git commit: boas práticas
    </a>
  </li>

  <li class="mv2">
    <a href="{{ site.url }}{% link slides/git_101_MAC0110.pdf %}" class="db pv1 link blue hover-mid-gray">
      <time class="fr silver ">Introduction to Computer Science Course at IME-USP (2020) </time>
	Git 101 (Portuguese)
    </a>
    By Matheus and Pedro Teos
  </li>

  <li class="mv2">
    <a href="{{ site.url }}{% link slides/threads_mutex_e_semaforos.pdf %}" class="db pv1 link blue hover-mid-gray">
      <time class="fr silver ">Parallel Programming Course at IME-USP (2019)</time>
	Threads, Mutex, e Semáforos (Portuguese)
    </a>
	By Alfredo Goldman, Giuliano Belinassi, and Matheus
  </li>

</ul>

<style>
.arec {
	color: brown !important;
	display: block;
}
</style>
