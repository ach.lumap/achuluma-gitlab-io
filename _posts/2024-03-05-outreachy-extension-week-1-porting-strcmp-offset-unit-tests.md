---
title: "Outreachy Week 13: Porting strcmp-offset unit tests"
tags: outreachy, git, testing, porting, t-strcmp-offset
---

## Review of Week 12: Review on unit-tests/t-hash.c
During Week 12 of the Outreachy internship, we made significant progress in the porting of the t-hash unit tests. Modifications included enhancing error messages, renaming test macros for clarity, and updating the strbuf_addstrings function for improved safety, efficiency, and error handling.

Additionally, we encountered a challenge related to the complexity of porting the t/t0013-sha1dc.sh test case for collision detection. After careful consideration, we decided to postpone its implementation to a later stage, allowing us to focus on advancing the porting of the t-hash unit tests.

A noteworthy change was made to the strbuf_addstrings function, where we replaced the usage of memcpy with strbuf_add for a more reliable and efficient approach. This change was made based on feedback received from the mailing list and code reviews.

## Porting t-strcmp-offset

Building on the progress from Week 12, our main focus in Week 13 is the migration of the unit tests for the strcmp-offset functionality to the new unit testing framework.

We introduced a new function called check_strcmp_offset. This function takes two strings, string1 and string2, and the expected result and offset values as parameters.
Here is the implementation of the check_strcmp_offset function:

```c
static void check_strcmp_offset(const char *string1, const char *string2, int expect_result, uintmax_t expect_offset)
{
    int result;
    size_t offset;

    result = strcmp_offset(string1, string2, &offset);

    /* Because different CRTs behave differently, only rely on signs of the result values. */
    result = (result < 0 ? -1 :
              result > 0 ? 1 :
              0);

    check_int(result, ==, expect_result);
    check_uint((uintmax_t)offset, ==, expect_offset);
}
```

This function enables us to compare the actual result and offset values with the expected ones, ensuring the correctness of the strcmp-offset functionality.

### Next steps

In the upcoming week, the primary objective is to  address any feedback received from Christian and prepare t-strcmp-offset for the mailing list.

Additionally, we will be rounding up the Outreachy internship program as it comes to an end.

Until next time,