---
title: "Outreachy Week 14: Update on strcmp-offset unit tests"
tags: outreachy, git, testing, porting, t-strcmp-offset, summary
---

## Review of Week 13: Review on unit-tests/t-hash.c
During Week 13 of the Outreachy internship, our main focus was on the migration of the unit tests for the strcmp-offset functionality.

## Updating strcmp-offset unit tests
In Week 14, we continued to refine the strcmp-offset unit tests to improve readability and avoid repetition. To achieve this, we introduced a test macro called TEST_STRCMP_OFFSET as follows:

```c
#define TEST_STRCMP_OFFSET(string1, string2, expect_result, expect_offset) \
    TEST(check_strcmp_offset(string1, string2, expect_result, expect_offset), \
        "strcmp_offset(%s, %s) works", #string1, #string2)
```

This test macro simplifies the process of testing the strcmp-offset functionality by providing a concise and reusable way to specify the input strings, expected result, and expected offset values.

## Submitting t-strcmp-offset patch to the mailing list
With the refined unit tests and improvements in place, we proceeded to submit the patch containing our changes to the project's mailing list for code review. This step is crucial for receiving feedback, addressing any potential issues, and ensuring the quality and maintainability of the codebase.

The patch submission marks an important milestone in the development process, as it allows for collaboration and validation of the changes made. The feedback received during the code review will guide us in making necessary adjustments and further improving the functionality.

## Next steps

As we approach the conclusion of the Outreachy internship, the upcoming weeks will involve the following steps:

- Address Code Review Feedback: I will carefully consider the feedback received on pending patches(t-date, t-advice, t-strcmp-offset, t-hash) and make any necessary adjustments or improvements. This iterative process ensures that the changes align with project standards and best practices.

- Finalize Pending Tasks: I will focus on completing any remaining tasks or outstanding items identified during the internship. This includes refining tests, addressing any open issues, and ensuring that all tests are correctly ported.

- Prepare a Summary Blog: To showcase the contributions made throughout the internship, I will prepare a comprehensive summary blog post that highlights all the significant milestones, accomplishments, and improvements achieved since the start of the internship. This summary will provide an overview of the project, the challenges faced, the solutions implemented, and the overall impact of the internship.

I am grateful for the opportunity to participate in this Outreachy internship, as it has been an incredible learning experience and a chance to contribute to the Git open-source project. I have gained valuable skills, collaborated effectively within a team, and deepened my understanding of software development best practices.

Stay tuned for the final update in the upcoming week as we conclude this enriching Outreachy internship.

Until next time,
