---
title: "Outreachy Week 10: Updates on unit-tests/t-hash.c"
tags: outreachy, git, testing, porting, t-hash, test-sha256, test-sha1
---

## Review of Week 9: Port helper/test-sha256.c and helper/test-sha1.c to unit-tests/t-hash.c

Last week, we engaged in porting helper/test-sha256.c and helper/test-sha1.c to unit-tests/t-hash.c".

## Updates on porting t-hash:

The `check_hash()` function is now adapted to account for NUL-terminated strings during tests.



```c
static void check_hash_data(const void *data, size_t data_length, const char *expected, int algo) {
	git_hash_ctx ctx;
	unsigned char hash[GIT_MAX_HEXSZ];
	const struct git_hash_algo *algop = &hash_algos[algo];

	if (!check(!!data)) {
    		test_msg("Error: No data provided");
    		return;
	}

	algop->init_fn(&ctx);
	algop->update_fn(&ctx, data, data_length);
	algop->final_fn(hash, &ctx);

	check_str(hash_to_hex_algop(hash, algop), expected);
}
```

Now we export an error when the wrong data is provided..

Furthermore, to avoid duplication and simplify readability, we encapsulated the test functions with the following macros:


```c
#define TEST_SHA1_STR(data, expected_output) \
    	TEST(check_hash_data(data, strlen(data), expected_output, GIT_HASH_SHA1), \
        	"SHA1 (%s) works", #data)

#define TEST_SHA1_DATA(data, expected_output) \
    	TEST(check_hash_data(data, (sizeof(data) - 1), expected_output, GIT_HASH_SHA1), \
        	"SHA1 (%s) works", #data)

#define TEST_SHA256_STR(data, expected_output) \
    	TEST(check_hash_data(data, strlen(data), expected_output, GIT_HASH_SHA256), \
        	"SHA256 (%s) works", #data)

#define TEST_SHA256_DATA(data, expected_output) \
    	TEST(check_hash_data(data, (sizeof(data) - 1), expected_output, GIT_HASH_SHA256), \
        	"SHA256 (%s) works", #data)
```

These macros provide a more convenient way to test both NUL-terminated strings and raw data for both SHA1 and SHA256 hash functions.

Last week, we used a `repeat()` function to account for large inputs with repeated patterns. However, after feedback from Christian, we decided to rename this function and move it to `strbuf.h` as an inline function.

```c
static inline void strbuf_addstrings(struct strbuf *sb, const char *s, size_t n)
{
    size_t len = strlen(s);
    strbuf_grow(sb, len * n);
    for (size_t i = 0; i < n; i++)
        strbuf_add(sb, s, len);
}

```
This inline function fits well with the strbuf family and improves  efficiency by allowing repeated string concatenation with minimal overhead.


### Next steps

In the upcoming week, the primary objective is to further refine the porting of the t-hash unit tests and address any feedback received during the review process.

Additionally, we will add the porting of t/t0013-sha1dc.sh for collision detection and efforts will be made to request and incorporate feedback from my mentor (Christian) to ensure the quality and accuracy of the ported tests.

Stay tuned for the next blog post, where we will provide updates on the progress of porting t-hash and highlight any further developments in the Outreachy internship.

Until next time,