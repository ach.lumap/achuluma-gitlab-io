---
title: "Outreachy Week 8: test-date.c Porting Completion and Final Patch Reviews"
tags: outreachy, git, testing, porting, t-date, patch reviews, test-sha256
---

## Review of Week 7: Test Porting Progress and Patch Reviews

In the seventh week of my Outreachy internship, some progress was made in the test-date.c porting process. The t-date test suite saw some major improvements.

Moreover, the community continued to provide valuable feedback and reviews for the previous patches submitted. A patch such as that of [t-ctype](https://lore.kernel.org/git/20231221231527.8130-1-ach.lumap@gmail.com/) was merged as well as that of . To view the specific changes and comments, please refer to the respective branches on the [github repo](https://github.com/achluma/git).

## Status update on t-date:

In t-date several code improvements were implemented to enhance readability and reduce repetition. Notably, macros were introduced to encapsulate common test case patterns.

```c
#define TEST_RELATIVE_DATE(value, expected_output) \
    TEST(check_relative_dates(value, expected_output), \
    "relative date (%s) works", #expected_output)

#define TEST_SHOW_DATE(format, time, expected, prereqs, zone) \
    TEST(check_show_date(format, time, expected, prereqs, zone), \
    "show date (%s) works", #format)

#define TEST_PARSE_DATE(given, expected_output, zone) \
    TEST(check_parse_date(given, expected_output, zone), \
    "parse date (%s) works", #expected_output)

#define TEST_APPROXIDATE(given, expected_output) \
    TEST(check_approxidate(given, expected_output), \
    "parse approxidate (%s) works", #given)

#define TEST_DATE_FORMAT_HUMAN(given, expected_output) \
    TEST(check_date_format_human(given, expected_output), \
    "human date (%s) works", #given)
```

These macros provide a more structured and concise way of writing test cases, making the code easier to understand and maintain.

Additionally, prior to the macro introductions, the did refactoring to create reusable functions for checking the 64-bit prerequisites.

```c
static inline int check_prereq_TIME_IS_64BIT(void) {
    return sizeof(timestamp_t) == 8;
}

static inline int check_prereq_TIME_T_IS_64BIT(void) {
    return sizeof(time_t) == 8;
}
```

These functions verify whether the required data types (timestamp_t and time_t) have a size of 8 bytes, indicating 64-bit support.
The functions were then used as follows to check for prerequisites in t-date.

```c
/* Macro to check prerequisites */
#define CHECK_PREREQ(var, prereq) \
    do { \
        if ((var) & prereq && !check_prereq_##prereq()) { \
            test_skip("missing prerequisite " #prereq); \
            return 0; \
        } \
    } while (0)

/* Return 1 if all prereqs are satisfied, 0 otherwise */
static int check_prereqs(unsigned int prereqs) {
    CHECK_PREREQ(prereqs, TIME_IS_64BIT);
    CHECK_PREREQ(prereqs, TIME_T_IS_64BIT);

    return 1;
}
```

Despite these improvements,  I am still facing challenges in handling the time zone (TZ) variable, in t-date.c. Specifically, two tests, which were initially passing in the t0006-date.sh  on Windows, fail after porting to the new framework. I have tried to set the timezone using the setenv, but unfortunately, the issue persists. I suspect that the problem ight be related to how Windows C compilers process the setenv function compared to POSIX environments.

However, the patch series for t-date has been pushed to the mailing list for further assistance as well as experience from developers who have experienced this issue before.

See some of the feedback from t-date on the mailing list [here](https://lore.kernel.org/git/20240205162506.1835-2-ach.lumap@gmail.com/)

### Next steps

In the upcoming week, the primary objective is to conclude the patch reviews and finalize the integration of t-date into the main codebase.

Furthermore, the focus will shift towards porting the test-sha256.c tests to the new testing framework. This module plays a crucial role in verifying the SHA-256 cryptographic algorithm implementation within the git codebase.

Stay tuned for the next blog post, where we will provide updates on the progress of porting the test-sha256.c module and highlight any further developments in the Outreachy internship.

Until next time,