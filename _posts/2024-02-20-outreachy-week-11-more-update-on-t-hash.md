---
title: "Outreachy Week 11: More changes on unit-tests/t-hash.c"
tags: outreachy, git, testing, porting, t-hash, test-sha256, test-sha1
---

## Review of Week 10: Update on unit-tests/t-hash.c

Last week, we engaged in updating "unit-tests/t-hash.c" to account for NUL-terminated strings during tests. We also encapsulated test functions with macros as well as move the `repeat()` funciton to `strbuf.h` as an inline `strbuf_addstrings()` function.

## Further Updates on unit-tests/t-hash.c
In the `check_hash()` when no data is provided (i.e., data is NULL), the error message is modified to include the expected hash value in the error message. The error message now reads: "Error: No data provided when expecting: [expected]".

```c
        if (!check(!!data)) {
		test_msg("Error: No data provided when expecting: %s", expected);
		return;
	}
```


The names of some test macros have also been changed to reflect the use.
```c
/* Only works with a literal string, useful when it contains a NUL character. */
#define TEST_SHA1_LITERAL(literal, expected) \
    	TEST(check_hash_data(literal, (sizeof(literal) - 1), expected, GIT_HASH_SHA1), \
        	"SHA1 (%s) works", #literal)

/* Only works with a literal string, useful when it contains a NUL character. */
#define TEST_SHA256_LITERAL(literal, expected) \
    	TEST(check_hash_data(literal, (sizeof(literal) - 1), expected, GIT_HASH_SHA256), \
        	"SHA256 (%s) works", #literal)
```

We updated `strbuf_addstrings` function to provide safety against memory overflow, efficiency through memcpy usage, explicit buffer length management, and clear error handling, making it a robust choice for adding repeated NUL-terminated strings to a buffer.

```c
void strbuf_addstrings(struct strbuf *sb, const char *s, size_t n)
{
	size_t len = strlen(s);
	if (unsigned_mult_overflows(len, n))
		die("you want to use way too much memory");
	strbuf_grow(sb, len * n);
	for (size_t i = 0; i < n; i++)
		memcpy(sb->buf + sb->len + len * i, s, len);
	strbuf_setlen(sb, sb->len + len * n);
}
```

### Next steps

In the upcoming week, the primary objective is to send the patch of the t-hash unit tests and address any feedback received during the review process.

Additionally, we will add the porting of t/t0013-sha1dc.sh for collision detection. Stay tuned for the next blog post, where we will provide updates on the progress of porting t-hash and highlight any further developments in the Outreachy internship.

Until next time,