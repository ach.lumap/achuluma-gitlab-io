---
title: "Outreachy Week 9: Port helper/test-sha256.c and helper/test-sha1.c to unit-tests/t-hash.c"
tags: outreachy, git, testing, porting, t-hash, patch reviews, test-sha256
---

## Review of Week 8: Test Porting Progress and Patch Reviews

In the eigth week of my Outreachy internship, we completed the porting of t-date unit tests and submitted to the mailing list for review and eventually merging..

## Porting t-hash:

This week we will port the unit tests for the hash functionality in git. This include the SHA1 and SHA256 hashing algorithm functions. A noteworthy function is the
`check_hash()` which verifies the hash functionality of SHA1 and SHA256.



```c
static void check_hash(const char *input, const char *expected, int algo) {
	git_hash_ctx ctx;
	unsigned char hash[GIT_MAX_HEXSZ];
	const struct git_hash_algo *algop = &hash_algos[algo];
	size_t data_length;

	if (!input || !*input) {
                test_msg("Error: No input provided");
		return;
	}

	algop->init_fn(&ctx);

	data_length = strlen(input);

	algop->update_fn(&ctx, input, data_length);

	algop->final_fn(hash, &ctx);

	check_str(hash_to_hex_algop(hash, algop), expected);
}
```

For now we display an error if the input is null or an empty string is supplied.

Also some tests made use of input strings with repeated patterns. So we added a repeat function to fascilitate the accuracy of the tests.

FROM

```shell
perl -e "$| = 1; print q{aaaaaaaaaa} for 1..100000;"

perl -e "$| = 1; print q{abcdefghijklmnopqrstuvwxyz} for 1..100000;"
```
TO

```c
static const char *repeat(const char *pattern, size_t repetitions) {
    	struct strbuf buf = STRBUF_INIT;

    	for (size_t i = 0; i < repetitions; i++) {
        	strbuf_addstr(&buf, pattern);
    	}

    	return buf.buf;
	strbuf_release(&buf);
}

.
.
.

aaaaaaaaaa_100000 = repeat("aaaaaaaaaa", 100000);
alphabet_100000 = repeat("abcdefghijklmnopqrstuvwxyz", 100000);
```

The porting process is going good so far, but it is pretty clear a lot could still be done to optimize the functions and accurately port the unit tests to the new framwork.


### Next steps

In the upcoming week, the primary objective is to improve on t-hash and followup the reviews of t-date on the mailing lists.

Furthermore, the focus will shift towards requesting and acting my mentor(Christian's) feedback. To view the specific changes and comments, please refer to the t256_* branches on the [github repo](https://github.com/achluma/git).

Stay tuned for the next blog post, where we will provide updates on the progress of porting t-hash and highlight any further developments in the Outreachy internship.

Until next time,