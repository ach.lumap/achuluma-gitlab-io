---
title: "Outreachy Week 12: Review on unit-tests/t-hash.c"
tags: outreachy, git, testing, porting, t-hash, test-sha256, test-sha1
---

## Review of Week 11: More changes on unit-tests/t-hash.c
Last week on unit-tests/t-hash.c, modifications include enhancing error messages in check_hash() for cases when no data is provided, renaming test macros for clarity, and updating strbuf_addstrings function for safety, efficiency, and error handling.

## Review on t-hash.c
Due to the complexity involved in working on the porting of t/t0013-sha1dc.sh for collision detection, we have decided to postpone its implementation for a later stage. Instead, our focus will remain on advancing the porting of t-hash unit tests.
Based on reviews from the mailing list, we had to change the `strbuf_addstrings`.

```c
void strbuf_addstrings(struct strbuf *sb, const char *s, size_t n)
{
       size_t len = strlen(s);
       if (unsigned_mult_overflows(len, n))
               die("you want to use way too much memory");
       strbuf_grow(sb, len * n);
       for (size_t i = 0; i < n; i++)
               strbuf_add(sb, s, len);
}
```
Instead of using memcpy to copy the string to the struct strbuf object , it now uses strbuf_add.
### Next steps

In the upcoming week, the focus will shift towards porting new tests to the new testing framework. Additionally, the internship officially closes this week, but we have been granted a two-week extension, so work will continue until then.

Until next time,


