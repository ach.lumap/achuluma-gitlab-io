---
title: " Outreachy Week 2: Take a step back"
tags: outreachy, git, testing, porting, t-date, t-ctype
---

This week I discussed about my previous task on porting test-date.c and started working on porting test-ctype.c

## Last Week's Follow-ups
Christian provided valuable advice regarding the porting of tests from t/helper and ensuring that the logic of the tests is maintained throughout the process. He emphasized the importance of preserving the integrity and reliability of the tests, as they play a crucial role in validating the behavior of the codebase.

During the porting of test-date.c, I encountered a run unit test error with the following message:
'Dubious, test returned 3 (wstat 768, 0x300)'

Despite my efforts, I couldn't fully understand the cause of this error. This experience highlighted the significance of seeking guidance and collaboration to effectively address such issues.

Christian's guidance has been instrumental in ensuring that the tests are properly ported and that their logic remains intact. I greatly appreciate his support and expertise in maintaining the quality and reliability of the test suite.

## Stepping back

After encountering the run unit test error when porting test-date.c, I sought guidance from Christian to address the issue. In the meantime, with Christian's advice, I decided to divert my focus to another task - specifically, working on porting test-ctype.c to validate the functionality of the character type functions. Christian provided further guidance by suggesting the use of a macro to avoid code duplication and ensure comprehensive testing of all character types.

Following Christian's advice, I implemented the following macro to test a character type:

```c
/* Macro to test a character type */
#define TEST_CTYPE_FUNC(func, string)            \
static void test_ctype_##func(void)              \
{                                               \
    int i;                                      \
    for (i = 0; i < 256; i++)                    \
        check_int(func(i), ==, is_in(string, i)); \
}
```

This macro allowed me to streamline the testing process and avoid redundant code. By using the macro, I could ensure that all character types were thoroughly tested, contributing to the overall reliability of the 't-ctype.c'.

## Difficulties
In code migration, navigating through its complexities often brings about various challenges. One such challenge lies in determining the appropriate moments to seek assistance when faced with intricate issues. Recognizing the boundaries of personal understanding and actively seeking mentorship is a vital skill. This particular week underscored the significance of efficient communication and seeking guidance from my mentor to swiftly and effectively overcome obstacles.

Moreover, encountering a run unit test error in t-date.c added an unexpected hurdle, requiring rethinking. Despite these challenges, each difficulty served as an opportunity for learning and fine-tuning the approach for a more streamlined migration process.

## Next Steps
- **Resolving Errors in t-date.c:** Addressing the run unit test error encountered in `t-date.c` remains a priority. Investigating the root cause and rectifying the issue to ensure smooth execution of unit tests is essential.

- **Patch Preparation for t-ctype.c:** A patch is slated for `t-ctype.c` to enhance its testing capabilities. This involves refining and augmenting the existing tests to bolster coverage and accuracy.

- **Continued Porting of Tests from t/helper:** The migration of tests from `t/helper` into the new testing framework is an ongoing effort. The objective is to systematically port tests, maintaining consistency and functionality.

The coming week is geared towards addressing these tasks, ensuring the successful migration of tests and maintaining the integrity of the testing suite.

Until next time,
