---
title: "Outreachy Week 7: Patch Reviews and Advancements in t-date"
tags: outreachy, git, testing, porting, t-date, patch reviews
---

## Review of Week 6: Patch Submissions and Collaboration

In the sixth week of my Outreachy internship, significant progress was made in the `t-ctype` test suite. Notable changes included the addition of tests to handle EOF (End of File). Additionally, work on the `t-advise` and `t-date` test suites continued, with ongoing collaboration and feedback from the community. You can take a look at the mailing
list throught the following links:

1- [t-ctype](https://lore.kernel.org/git/20231221231527.8130-1-ach.lumap@gmail.com/)
2- [t-advise](https://lore.kernel.org/git/20240112102122.1422-1-ach.lumap@gmail.com/)

Generally, my mentors reviews are on my [github repo](https://github.com/achluma/git). Each change between reviews corresponds to a branch and the name of the branch is indicative of the test being ported.

## Status update on t-date:
During the porting process, several notable changes were made to the `t-date` test suite, including improvements to the handling of the time zone variable. One significant improvement was modifying the code snippet responsible for setting the time zone environment variable. The desired behavior is to call `set_TZ_env(zone)` only when the time zone was not an empty string. To achieve this, the code was updated to use `strcmp()` within an `if` condition, like this:

```c
if (!strcmp(zone, ""))
    set_TZ_env(zone);
```

An additional change was the adoption of the xstrfmt() function instead of snprintf(). The xstrfmt() function provides a safer and more convenient way to format strings by automatically allocating memory as needed. By using xstrfmt(), the codebase benefits from improved memory management and reduced risks associated with buffer overflows.

Finally, it was suggested to split the TEST(...) lines into multiple lines to avoid excessively long lines. This approach enhances code readability and reduces the likelihood of errors or inconsistencies. For example, instead of having a single long line for a test case, it is recommended to split it into multiple lines, each containing a specific parameter or description. This formatting improvement contributes to cleaner and more comprehensible code.

```c
TEST(check_relative_dates(5, "5 seconds ago"),
	"Check relative date - 5 seconds ago works");
```

### Next Steps

Moving forward, the focus will be on creating patches for t-date.c and porting other identified test cases to the new framework.

Stay tuned for more updates in the next blog post, where we will delve deeper into the test porting process and its impact on the overall testing efforts.

Until next time,